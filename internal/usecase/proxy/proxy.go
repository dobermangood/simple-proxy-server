package proxy

import (
	"context"

	"gitlab.com/dobermangood/simple-proxy-server/internal/entity"
)

type WebApi interface {
	Send(ctx context.Context, inputProxy entity.InputProxy) (entity.OutputProxy, error)
}

type Repo interface {
	SaveInputProxy(inputProxy entity.InputProxy) error
	SaveOutputProxy(outputProxy entity.OutputProxy) error
}

type UseCase struct {
	wepapi WebApi
	repo   Repo
}

func New(wepapi WebApi, repo Repo) *UseCase {
	return &UseCase{
		wepapi: wepapi,
		repo:   repo,
	}
}

func (uc *UseCase) SendRequest(ctx context.Context, inputProxy entity.InputProxy) (entity.OutputProxy, error) {
	outputProxy, err := uc.wepapi.Send(ctx, inputProxy)
	if err != nil {
		return outputProxy, err
	}

	err = uc.repo.SaveInputProxy(inputProxy)
	if err != nil {
		return outputProxy, err
	}

	err = uc.repo.SaveOutputProxy(outputProxy)
	if err != nil {
		return outputProxy, err
	}

	return outputProxy, nil
}
