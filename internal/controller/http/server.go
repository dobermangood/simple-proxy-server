package http

import (
	"net/http"

	"gitlab.com/dobermangood/simple-proxy-server/internal/controller/http/proxy"
)

func RegisterRoutes(
	server *http.Server,
	proxyUseCase proxy.UseCase,
) {
	proxyHandler := proxy.New(proxyUseCase)
	http.HandleFunc("/proxy", proxyHandler.ProxyHandlerFunc)
}
