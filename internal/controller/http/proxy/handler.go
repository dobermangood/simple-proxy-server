package proxy

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/dobermangood/simple-proxy-server/internal/entity"
)

type UseCase interface {
	SendRequest(ctx context.Context, inputProxy entity.InputProxy) (entity.OutputProxy, error)
}

type Handler struct {
	useCase UseCase
}

func New(useCase UseCase) *Handler {
	return &Handler{
		useCase: useCase,
	}
}

func (ph *Handler) ProxyHandlerFunc(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	inputProxy := entity.InputProxy{}

	err := json.NewDecoder(r.Body).Decode(&inputProxy)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	outputProxy, err := ph.useCase.SendRequest(r.Context(), inputProxy)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonResponse, err := json.Marshal(outputProxy)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonResponse)
}
