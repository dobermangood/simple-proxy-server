package app

import (
	"context"
	"net/http"

	httpController "gitlab.com/dobermangood/simple-proxy-server/internal/controller/http"
	repoProxy "gitlab.com/dobermangood/simple-proxy-server/internal/infrastructure/repo/memory/proxy"
	webapiProxy "gitlab.com/dobermangood/simple-proxy-server/internal/infrastructure/wepapi/proxy"
	useCaseProxy "gitlab.com/dobermangood/simple-proxy-server/internal/usecase/proxy"
	"gitlab.com/dobermangood/simple-proxy-server/pkg/httpclient"
	"gitlab.com/dobermangood/simple-proxy-server/pkg/httpserver"
)

type App struct {
	ctx        context.Context
	httpServer *http.Server
}

func New(ctx context.Context) *App {
	httpClient := httpclient.New()
	httpServer := httpserver.New()

	proxyWebApi := webapiProxy.New(httpClient)
	proxyRepo := repoProxy.New()
	proxyUseCase := useCaseProxy.New(proxyWebApi, proxyRepo)

	httpController.RegisterRoutes(httpServer, proxyUseCase)

	return &App{
		ctx:        ctx,
		httpServer: httpServer,
	}
}

func (a *App) Run() error {
	err := a.httpServer.ListenAndServe()
	return err
}

func (a *App) Stop() error {
	err := a.httpServer.Shutdown(a.ctx)
	return err
}
