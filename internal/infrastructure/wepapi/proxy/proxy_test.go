package proxy

import (
	"bytes"
	"context"
	"io/ioutil"
	"math/rand"
	"net/http"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/dobermangood/simple-proxy-server/internal/entity"
)

type RoundTripFunc func(req *http.Request) *http.Response

func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func newTestClient(fn RoundTripFunc) *http.Client {
	return &http.Client{
		Transport: RoundTripFunc(fn),
	}
}

func TestSend_Mock(t *testing.T) {
	testCases := []struct {
		name string
		in   entity.InputProxy
		out  entity.OutputProxy
	}{
		{
			name: "get habr.com 200",
			in: entity.InputProxy{
				Method: "GET",
				Url:    "https://habr.com",
				Headers: map[string]string{
					"Content-Type":  "application/json",
					"Authorization": "Basic test",
				},
			},
			out: entity.OutputProxy{
				ID:     "",
				Status: 200,
				Headers: map[string]string{
					"Content-Type":  "application/json",
					"Authorization": "Basic test",
				},
				Length: 50,
			},
		},
		{
			name: "get habr.com 500",
			in: entity.InputProxy{
				Method: "GET",
				Url:    "https://habr.com",
				Headers: map[string]string{
					"Content-Type": "application/json",
				},
			},
			out: entity.OutputProxy{
				ID:      "",
				Status:  500,
				Headers: make(map[string]string),
				Length:  0,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			client := newTestClient(func(req *http.Request) *http.Response {
				header := make(http.Header)

				for key, value := range tc.out.Headers {
					header.Add(key, value)
				}

				return &http.Response{
					StatusCode: tc.out.Status,
					Body:       ioutil.NopCloser(bytes.NewBuffer(randStringBytes(tc.out.Length))),
					Header:     header,
				}
			})

			ctx := context.Background()
			webApi := New(*client)
			out, err := webApi.Send(ctx, tc.in)
			require.NoError(t, err)

			// check correct uuid
			_, err = uuid.Parse(out.ID)
			require.NoError(t, err)
			out.ID = ""

			require.EqualValues(t, tc.out, out)
		})
	}
}

func TestSend_Real(t *testing.T) {
	testCases := []struct {
		name string
		in   entity.InputProxy
		out  entity.OutputProxy
	}{
		{
			name: "get jsonplaceholder.com 200",
			in: entity.InputProxy{
				Method: "GET",
				Url:    "https://jsonplaceholder.typicode.com/users",
			},
			out: entity.OutputProxy{
				Status: 200,
				Length: 5645,
			},
		},
		{
			name: "get jsonplaceholder.com 404",
			in: entity.InputProxy{
				Method: "GET",
				Url:    "https://jsonplaceholder.typicode.com/users/-1",
			},
			out: entity.OutputProxy{
				Status:  404,
				Headers: make(map[string]string),
				Length:  2,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			client := http.Client{}

			ctx := context.Background()
			webApi := New(client)
			out, err := webApi.Send(ctx, tc.in)
			require.NoError(t, err)

			// check correct uuid
			_, err = uuid.Parse(out.ID)
			require.NoError(t, err)

			require.Equal(t, tc.out.Status, out.Status)
			require.Equal(t, tc.out.Length, out.Length)
		})
	}
}

func randStringBytes(n int) []byte {
	letterBytes := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return b
}
