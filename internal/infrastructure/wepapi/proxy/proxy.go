package proxy

import (
	"context"
	"io"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/dobermangood/simple-proxy-server/internal/entity"
)

type WepApi struct {
	client http.Client
}

func New(client http.Client) *WepApi {
	return &WepApi{
		client: client,
	}
}

func (wa *WepApi) Send(ctx context.Context, inputProxy entity.InputProxy) (entity.OutputProxy, error) {
	outputProxy := entity.OutputProxy{}

	req, err := http.NewRequestWithContext(ctx, inputProxy.Method, inputProxy.Url, nil)
	if err != nil {
		return outputProxy, nil
	}

	for key, value := range inputProxy.Headers {
		req.Header.Set(key, value)
	}

	res, err := wa.client.Do(req)
	if err != nil {
		return outputProxy, nil
	}

	defer res.Body.Close()

	id, err := uuid.NewRandom()
	if err != nil {
		return outputProxy, nil
	}

	outputProxy.ID = id.String()
	outputProxy.Status = res.StatusCode

	outputProxy.Headers = make(map[string]string)
	for key, values := range res.Header {
		for _, value := range values {
			outputProxy.Headers[key] = value
		}
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		return outputProxy, nil
	}
	outputProxy.Length = len(resBody)

	return outputProxy, nil
}
