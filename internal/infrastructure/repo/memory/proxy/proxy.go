package proxy

import (
	"sync"

	"gitlab.com/dobermangood/simple-proxy-server/internal/entity"
)

type Repo struct {
	mu             sync.Mutex
	inputRequests  []*entity.InputProxy
	outputRequests []*entity.OutputProxy
}

func New() *Repo {
	return &Repo{}
}

func (r *Repo) SaveInputProxy(inputProxy entity.InputProxy) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.inputRequests = append(r.inputRequests, &inputProxy)
	return nil
}

func (r *Repo) SaveOutputProxy(outputProxy entity.OutputProxy) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.outputRequests = append(r.outputRequests, &outputProxy)
	return nil
}
