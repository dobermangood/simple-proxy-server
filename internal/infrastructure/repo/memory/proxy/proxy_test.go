package proxy

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dobermangood/simple-proxy-server/internal/entity"
)

func TestSaveInputProxy(t *testing.T) {
	repo := New()
	testInputProxy := entity.InputProxy{}
	testInputProxyCount := 50

	wg := &sync.WaitGroup{}
	for i := 0; i < testInputProxyCount; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			repo.SaveInputProxy(testInputProxy)
			wg.Done()
		}(wg)
	}

	wg.Wait()

	require.Equal(t, testInputProxyCount, len(repo.inputRequests))
}

func TestSaveOutputProxy(t *testing.T) {
	repo := New()
	testOutputProxy := entity.OutputProxy{}
	testOutputProxyCount := 50

	wg := &sync.WaitGroup{}
	for i := 0; i < testOutputProxyCount; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			repo.SaveOutputProxy(testOutputProxy)
			wg.Done()
		}(wg)
	}

	wg.Wait()

	require.Equal(t, testOutputProxyCount, len(repo.outputRequests))
}
