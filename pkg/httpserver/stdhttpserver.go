package httpserver

import "net/http"

func New() *http.Server {
	return &http.Server{
		Addr: ":3000", // TODO: extract to config
	}
}
