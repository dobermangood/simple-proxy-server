package main

import (
	"context"
	"log"

	"gitlab.com/dobermangood/simple-proxy-server/internal/app"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	server := app.New(ctx)
	defer server.Stop()

	// TODO: gracefull shutdown
	if err := server.Run(); err != nil {
		log.Fatal(err)
	}
}
